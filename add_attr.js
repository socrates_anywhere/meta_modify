module.exports.setAttribute = function (FileName, Element, Attribute) {
    var DOMParser = require('xmldom').DOMParser;
    var XMLSerializer = require('xmldom').XMLSerializer;
    var fs = require('fs');
    var inflect = require('i')();
    
    fd = fs.readFileSync(FileName,'utf8'); 
  
    var doc = new DOMParser().parseFromString(fd, 'text/xml');
    if (doc.getElementsByTagName(Element).length === 0 ) {
    	console.log("No Element in   "+ FileName + " !");
    } else if (doc.getElementsByTagName(Element)[0].getAttribute("Name").length === 0) {
        console.log("No Attribute in "+ FileName + " !");
    } else {
    	console.log("matched in      " + FileName + " !" );
        x=doc.getElementsByTagName(Element);
        // each element 
        for (i=0;i<x.length;i++) {
            //add arrtibute and change the value to plural form       
            x[i].setAttribute( Attribute, inflect.pluralize(doc.getElementsByTagName(Element)[0].getAttribute("Name")) );
        }
        var xml_temp = (new XMLSerializer()).serializeToString(doc);
        var xml_new = xml_temp.replace("?>","?>\n");
    	fs.writeFileSync(FileName, xml_new);
    }
}

