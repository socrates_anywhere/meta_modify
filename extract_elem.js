module.exports.extractElement = function (FileName, NewFileName, Element) {
    var DOMParser = require('xmldom').DOMParser;
    var XMLSerializer = require('xmldom').XMLSerializer;
    var fs = require('fs');
    var child = new Array();

    fd = fs.readFileSync(FileName,'utf8'); 
    fd_new = fs.readFileSync(NewFileName, 'utf8');

    var doc = new DOMParser().parseFromString(fd, 'text/xml');
    var doc_new = new DOMParser().parseFromString(fd_new, 'text/xml');
    if (doc.getElementsByTagName(Element).length === 0 ) {
        console.log("No Element in   "+ FileName + " !");
    } else {
        console.log("matched in      " + FileName + " !" );    
        x=doc.getElementsByTagName(Element);
        for ( i=0;i<x.length;i++ ) {     
            // insert new node to the new file
            var has_element = 0;
            y=doc_new.getElementsByTagName("ComplexType");
            for ( j=0;j<y.length;j++ ) {
                if (x[i].getAttribute("Name")==y[j].getAttribute("Name")) {
                    has_element = 1;
                }       
            }  
            if (!has_element) {
                temp=doc_new.getElementsByTagName("ComplexTypes")[0];
                temp.appendChild(x[i]);
            } 
        }
        var xml_temp = (new XMLSerializer()).serializeToString(doc_new);
        // add enter
        var xml_new = xml_temp.replace(/></g,">\n        <");
        fs.writeFileSync(NewFileName, xml_new);
    }
}